# PyTorch2.0.1 Ubuntu20.04LTS ARM64

[![](assets/readme/Author-170479530610711.svg)]() [![](assets/readme/Organization-170479530610712.svg)]() [![](assets/readme/License-170479530610713.svg)]() [![](assets/readme/Chat-170479530610814.svg)](https://gitter.im/leaveking/xunsi-note) [![](assets/readme/Time-170479530610815.svg)]() 

---

## 简介

这是一个基本的Python环境，适用于以下情况：

| 条目          | 型号/版本                                                    | 备注                                                        |
| ------------- | ------------------------------------------------------------ | ----------------------------------------------------------- |
| 硬件          | 华为昇腾Atlas800-9000服务器                                  | NPU910B、鲲鹏ARM64架构CPU                                   |
| 系统          | Ubuntu20.04                                                  | 内核必须为：Linux guest 5.4.0-26-generic                    |
| 物理机NPU驱动 | Ascend-hdk-910-npu-driver_23.0.rc2_linux-aarch64.run<br/>Ascend-hdk-910-npu-firmware_6.4.12.1.241.run<br/>Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run<br/>Ascend-docker-runtime_5.0.RC2_linux-aarch64.run | 版本号必须严格对应                                          |
| Python        | Python3.8.18                                                 | 要求3.8版本                                                 |
| 容器内CANN    | Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run                |                                                             |
| 神经网络框架  | PyTorch2.0.1                                                 | [Ascend torch_npu适配版](https://github.com/Ascend/pytorch) |

## 准备工作

查看当前Atlas服务器物理机，系统内核是否如下：

```bash
# 要求系统内核版本严格对应，包括小版本号（这个版本的Pytorch导致的）
# uname -a
Linux guest 5.4.0-26-generic #30-Ubuntu SMP Mon Apr 20 16:59:40 UTC 2020 aarch64 aarch64 aarch64 GNU/Linux
```

查看是否安装了以下NPU驱动：

```bash
Ascend-hdk-910-npu-driver_23.0.rc2_linux-aarch64.run
Ascend-hdk-910-npu-firmware_6.4.12.1.241.run
Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run
Ascend-docker-runtime_5.0.RC2_linux-aarch64.run
```

如果没有安装，到下面的百度网盘链接，下载软件包，并安装：

```bash
# 链接：https://pan.baidu.com/s/19yBuQoFiyDWZWcb4y7pgTQ 
# 提取码：8aqd

# ./910B驱动 --full --install-for-all
./Ascend-hdk-910-npu-driver_23.0.rc2_linux-aarch64.run --full --install-for-all

# ./910B固件 --full
./Ascend-hdk-910-npu-firmware_6.4.12.1.241.run --full

# ./910B的CANN --install --install-for-all
./Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run --install --install-for-all

./Ascend-docker-runtime_5.0.RC2_linux-aarch64.run --install
```

将 `Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run` 软件包，复制到当前文件夹内（`pytorch/2.0.1/ubuntu20.04-arm64`）。

## Python

创建一个Docker镜像，这个环境安装了适用于 [ascend pytorch v2.0.1](https://github.com/Ascend/pytorch/tree/v2.0.1) 的Python版本：

```bash
# cd pytorch/2.0.1/ubuntu20.04-arm64
# docker build -t 用户名/名称:版本号(版本-系统版本-CPU架构) .(注意这里有个点)
docker build -t nuvic/ascendpython:3.8.18-ubuntu20.04-arm64 -f python/Dockerfile .
```

## CANN

基于上一步创建的 `ascendpython:3.8.18-ubuntu20.04-arm64` 创建一个新的Docker镜像，新的镜像将安装适用于  [ascend pytorch v2.0.1](https://github.com/Ascend/pytorch/tree/v2.0.1) 的CANN版本：

```bash
# cd pytorch/2.0.1/ubuntu20.04-arm64

version=nuvic/ascendcann:6.3.RC2-ubuntu20.04-arm64
base=nuvic/ascendpython:3.8.18-ubuntu20.04-arm64
# 执行下面命令之前，请确保Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run文件在当前目录内
docker build -t ${version} --build-arg BASE=${base} -f cann/Dockerfile .
```

## PyTorch

基于上一步创建的 `ascendcann:6.3.RC2-ubuntu20.04-arm64`，创建一个新的Docker镜像，新的镜像将安装  [ascend pytorch v2.0.1](https://github.com/Ascend/pytorch/tree/v2.0.1) :

```bash
# cd pytorch/2.0.1/ubuntu20.04-arm64

version=nuvic/ascendpytorch:2.0.1-ubuntu20.04-arm64
base=nuvic/ascendcann:6.3.RC2-ubuntu20.04-arm64

docker build -t ${version} --build-arg BASE=${base} -f pytorch/Dockerfile .
```

## 完成测试

上面创建完成了 `ascendpytorch:2.0.1-ubuntu20.04-arm64` 镜像，使用下面命令进行启动：

```bash
image_tag=ascendpytorch:2.0.1-ubuntu20.04-arm64
container_name=ascendpytorch

# 使用镜像创建容器
docker run \
-itd -u 0 \
--privileged \
--ipc=host \
--net=host \
--device=/dev/davinci6 \
--device=/dev/davinci_manager \
--device=/dev/devmm_svm \
--device=/dev/hisi_hdc \
-v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
-v /usr/local/Ascend/add-ons/:/usr/local/Ascend/add-ons/ \
-v /home/installFiles:/home/installFiles \
-v /usr/local/dcmi:/usr/local/dcmi \
-v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
--name=${container_name} ${image_tag} /bin/bash
```

然后物理机上，执行下面命令，登录容器：

```bash
docker exec -it ascendpytorch bash
```

然后在容器内，执行下面命令，进行测试：

```bash
npu-smi info
```

打印结果如下，表明NPU驱动正常：

```bash
+------------------------------------------------------------------------------------------------+
| npu-smi 23.0.rc2                 Version: 23.0.rc2                                             |
+---------------------------+---------------+----------------------------------------------------+
| NPU   Name                | Health        | Power(W)    Temp(C)           Hugepages-Usage(page)|
| Chip                      | Bus-Id        | AICore(%)   Memory-Usage(MB)  HBM-Usage(MB)        |
+===========================+===============+====================================================+
| 0     910B                | OK            | 70.7        44                0    / 0             |
| 0                         | 0000:C1:00.0  | 0           1186 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 1     910B                | OK            | 67.0        41                0    / 0             |
| 0                         | 0000:81:00.0  | 0           1830 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 2     910B                | OK            | 67.6        41                0    / 0             |
| 0                         | 0000:41:00.0  | 0           3225 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 3     910B                | OK            | 67.3        43                0    / 0             |
| 0                         | 0000:01:00.0  | 0           1961 / 15039      1    / 32768         |
+===========================+===============+====================================================+
| 4     910B                | OK            | 70.2        43                0    / 0             |
| 0                         | 0000:C2:00.0  | 0           1336 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 5     910B                | OK            | 68.3        40                0    / 0             |
| 0                         | 0000:82:00.0  | 0           3187 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 6     910B                | OK            | 69.0        42                0    / 0             |
| 0                         | 0000:42:00.0  | 0           2301 / 15137      1    / 32768         |
+===========================+===============+====================================================+
| 7     910B                | OK            | 67.7        44                0    / 0             |
| 0                         | 0000:02:00.0  | 0           1387 / 15039      1    / 32768         |
+===========================+===============+====================================================+
```

在执行下面命令，看看NPU是否能被正常调用：

```bash
python /root/test.py
```

打印结果如下,，表明NPU被正常调用：

```bash
tensor([[1., 1., 1.],
        [1., 1., 1.]], device='npu:0')
```

## DockerHub保存

将创建好的镜像，推送到docker hub上保存。首先在web页面，登录你的 [hub.docker.com](https://hub.docker.com/) ，并创建一个repository：

```bash
weiwenying/ascendpytorch
```

然后在命令行，登录docker hub:

```bash
docker login 
# 输入账号(例如: nuvic)
# 输入密码：******
```

然后进行推送：

```bash
# 先创建路径
# docker tag ascendpytorch:版本 dockerhub用户名/ascendpytorch:版本
# docker tag ascendpytorch:2.0.1-ubuntu20.04-arm64 nuvic/ascendpytorch:2.0.1-ubuntu20.04-arm64
# 推送
docker push nuvic/ascendpytorch:2.0.1-ubuntu20.04-arm64
```

