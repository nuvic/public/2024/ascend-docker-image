#  Python environment on Ascend Atlas NPU Server

`作者：魏文应`  `时间：2024-01-11`

---

## 3.8.18-ubuntu20.04-arm64

本镜像环境安装了适用于 [ascend pytorch v2.0.1](https://github.com/Ascend/pytorch/tree/v2.0.1) 的Python版本，适用于以下情况：

| 条目          | 型号/版本                                                    | 备注                                                        |
| ------------- | ------------------------------------------------------------ | ----------------------------------------------------------- |
| 硬件          | 华为昇腾Atlas800-9000服务器                                  | NPU910B、鲲鹏ARM64架构CPU                                   |
| 系统          | Ubuntu20.04                                                  | 内核必须为：Linux guest 5.4.0-26-generic                    |
| 物理机NPU驱动 | Ascend-hdk-910-npu-driver_23.0.rc2_linux-aarch64.run<br/>Ascend-hdk-910-npu-firmware_6.4.12.1.241.run<br/>Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run<br/>Ascend-docker-runtime_5.0.RC2_linux-aarch64.run | 版本号必须严格对应                                          |
| Python        | Python3.8.18                                                 | 要求3.8版本                                                 |
| 容器内CANN    | Ascend-cann-toolkit_6.3.RC2_linux-aarch64.run                |                                                             |
| 神经网络框架  | PyTorch2.0.1                                                 | [Ascend torch_npu适配版](https://github.com/Ascend/pytorch) |

使用下面命令进行启动：

```bash
image=ascendpython:3.8.18-ubuntu20.04-arm64
container=ascendpython

# 使用镜像创建容器
docker run --name=${container} ${image} /bin/bash
```

然后物理机上，执行下面命令，登录容器：

```bash
docker exec -it ${container} /bin/bash
```

然后在容器内，执行下面命令，进入python进行测试，看看python是否正常：

```bash
python
```