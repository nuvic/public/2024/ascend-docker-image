# Ascend Docker Image

[![](assets/README/Author.svg)]() [![](assets/README/Organization.svg)]() [![](assets/README/License.svg)]() [![](assets/README/Chat.svg)](https://gitter.im/leaveking/xunsi-note) [![](assets/README/Time.svg)]() 

---

## 简介

昇腾Atlas800-9000服务器，配备910NPU。本项目用于制作相应镜像。本项目是官方文档的精简版，更多内容，请参考官方项目 [ascend-docker-image](https://github.com/Ascend/ascend-docker-image) 镜像制作方法

## 本项目镜像

本项目提供的可选镜像：
- [pytorch/2.0.1/ubuntu20.04-arm64](pytorch/2.0.1/ubuntu20.04-arm64)  ;
